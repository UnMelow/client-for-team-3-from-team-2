from copy import deepcopy

import game.main as game
from game.classes import Board

WHITE = "white"
BLACK = "black"
BOARD = Board()
game.initialize(BOARD)


def minimax(board, max_player, black, depth=2):
    color = BLACK if black else WHITE
    if depth == 0 or game.is_game_finished(board, False):
        return game.count_figures(board)[0] - game.count_figures(board)[1], board

    if max_player:
        max_eval = -200
        best_move = None
        for moves in get_all_moves(color, board):
            evaluation = minimax(moves[0], False, not black, depth - 1)[0]
            max_eval = max(max_eval, evaluation)
            if max_eval == evaluation:
                best_move = moves[1]

        return max_eval, best_move
    else:
        min_eval = 200
        best_move = None
        for moves in get_all_moves(color, board):
            evaluation = minimax(moves[0], True, not black, depth - 1)[0]
            min_eval = min(min_eval, evaluation)
            if min_eval == evaluation:
                best_move = moves[1]

        return min_eval, best_move


def simulate_move(move, board, is_jump):
    if is_jump:
        row, col = game.string_to_coordinates(move[0])
        game.apply_capture(board, game.get_captures(board, row, col))
    else:
        game.apply_move(board, move)
    return board


def get_all_moves(color, board):
    moves = []
    is_jump = True
    valid_moves = game.get_hints(board, color)
    if valid_moves[0]:
        valid_moves = valid_moves[0]
        is_jump = False
    else:
        valid_moves = valid_moves[1]
    for piece in valid_moves:
        temp_board = deepcopy(board)
        move = (piece[0], piece[1])
        new_board = simulate_move(move, temp_board, is_jump)
        moves.append((new_board, (move, is_jump)))
    return moves
