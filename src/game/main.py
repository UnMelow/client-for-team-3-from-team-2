import copy
import random
from typing import Tuple, List

from game.classes import Figure, Board

down, up = [(+1, -1), (+1, +1)], [(-1, -1), (-1, +1)]


def get_moves(
    board: Board(), row: int, col: int, is_sorted: bool = False
) -> List[List[str]]:
    """The function returns a list of all possible moves for a unique figure."""
    length = board.get_size()
    figure = board.get_cell(row, col)
    result = []
    if figure:
        bottom = [
            coordinates_to_string(row + x, col + y)
            for (x, y) in down
            if (0 <= (row + x) < length)
            and (0 <= (col + y) < length)
            and board.cell_is_free(row + x, col + y)
        ]
        top = [
            coordinates_to_string(row + x, col + y)
            for (x, y) in up
            if (0 <= (row + x) < length)
            and (0 <= (col + y) < length)
            and board.cell_is_free(row + x, col + y)
        ]
        result = (
            (
                sorted(bottom + top)
                if figure.is_king()
                else (sorted(bottom) if figure.is_black() else sorted(top))
            )
            if is_sorted
            else (
                bottom + top
                if figure.is_king()
                else (bottom if figure.is_black() else top)
            )
        )
    return result


def get_jumps(board: Board(), row: int, col: int, is_sorted: bool = False) -> List[str]:
    """The function returns a list of all possible captures for a unique figure."""
    length = board.get_size()
    figure = board.get_cell(row, col)
    if figure:
        bottom = [
            coordinates_to_string(row + 2 * x, col + 2 * y)
            for (x, y) in down
            if (0 <= (row + 2 * x) < length)
            and (0 <= (col + 2 * y) < length)
            and board.cell_is_free(row + 2 * x, col + 2 * y)
            and (not board.cell_is_free(row + x, col + y))
            and (board.get_cell(row + x, col + y).get_color() != figure.get_color())
        ]
        top = [
            coordinates_to_string(row + 2 * x, col + 2 * y)
            for (x, y) in up
            if (0 <= (row + 2 * x) < length)
            and (0 <= (col + 2 * y) < length)
            and board.cell_is_free(row + 2 * x, col + 2 * y)
            and (not board.cell_is_free(row + x, col + y))
            and (board.get_cell(row + x, col + y).get_color() != figure.get_color())
        ]
        return (
            (
                sorted(bottom + top)
                if figure.is_king()
                else (sorted(bottom) if figure.is_black() else sorted(top))
            )
            if is_sorted
            else (
                bottom + top
                if figure.is_king()
                else (bottom if figure.is_black() else top)
            )
        )
    return []


def search_path(
    board: Board(), row: int, col: int, path: List, paths: List, is_sorted: bool = False
) -> None:
    """
    This function recursive builds all capturing paths started at a certain
    row/col position.
    """
    path.append(coordinates_to_string(row, col))
    jumps = get_jumps(board, row, col, is_sorted)
    if not jumps:
        paths.append(path)
    else:
        for position in jumps:
            (row_to, col_to) = string_to_coordinates(position)
            piece = copy.copy(board.get_cell(row, col))
            board.delete_cell(row, col)
            board.set_cell(row_to, col_to, piece)
            if (
                (piece.get_color() == "black" and row_to == board.get_size() - 1)
                or (piece.get_color() == "white" and row_to == 0)
                and (not piece.is_king())
            ):
                piece.make_king()
            row_mid = row + 1 if row_to > row else row - 1
            col_mid = col + 1 if col_to > col else col - 1
            capture = board.get_cell(row_mid, col_mid)
            board.delete_cell(row_mid, col_mid)
            search_path(board, row_to, col_to, copy.copy(path), paths)
            board.set_cell(row_mid, col_mid, capture)
            board.delete_cell(row_to, col_to)
            board.set_cell(row, col, piece)


def get_captures(board: Board(), row: int, col: int, is_sorted: bool = False) -> List:
    """The function return a list of all possible paths captures"""
    paths = []
    board_ = copy.copy(board)
    search_path(board_, row, col, [], paths, is_sorted)
    if len(paths) == 1 and len(paths[0]) == 1:
        paths = []
    return paths


def random_color_distribution() -> Tuple[str, str]:
    """Function randomly selects and assigns colors to players"""
    first_choice = random.choice(("white", "black"))
    if first_choice == "white":
        return first_choice, "black"
    return first_choice, "white"


usage = """
    Usage:
        pass:       give up, admit defeat and exit the game
        move x y:   moves a figure from x to y
        jump x y:   jumps a figure from x to y
"""


def string_to_coordinates(position: str) -> Tuple[int, int]:
    """The Function Converts a string of coordinates like 'a1' to coordinates in numbers."""
    return ord(position[0]) - ord("a"), int(position[1:]) - 1


def coordinates_to_string(row: int, col: int) -> str:
    """The function converts the coordinates to a string of the form like 'a1'."""
    return chr(row + 97) + str(col + 1)


def initialize(board: Board) -> None:
    """
    Initial filling of the field.
    """
    row = col = board.get_size()
    for i in range(row - 1, row // 2, -1):
        for j in range(0 if i % 2 == 1 else 1, col, 2):
            board.set_cell(i, j, Figure("white"))
    for i in range(0, row // 2 - 1):
        for j in range(0 if i % 2 == 1 else 1, col, 2):
            board.set_cell(i, j, Figure("black"))


def count_figures(board: Board) -> Tuple[int, int]:
    """The function counts the number of black and white figures on the field."""
    row = column = board.get_size()
    black, white = 0, 0
    for i in range(row):
        for j in range(column):
            figure = board.get_cell(i, j)
            if figure:
                if figure.is_black():
                    black += 0.5 if figure.is_king() else 1
                elif figure.is_white():
                    white += 0.5 if figure.is_king() else 1
    return black, white


def get_all_moves(
    board: Board, color: str, is_sorted: bool = False
) -> List[Tuple[str, List[str]]]:
    """All possible moves for the transferred color."""
    row = col = board.get_size()
    result = []
    for i in range(row):
        for j in range(col):
            figure = board.get_cell(i, j)
            if figure:
                if figure.get_color() == color:
                    path_list = get_moves(board, i, j, is_sorted)
                    path_start = coordinates_to_string(i, j)
                    for path in path_list:
                        result.append((path_start, path))

    if is_sorted:
        result.sort()
    return result


def sort_captures(all_captures: List, is_sorted: bool = False) -> List:
    """Function sorts the captures so that you can build the paths of them"""
    return (
        sorted(all_captures, key=lambda x: (-len(x), x[0]))
        if is_sorted
        else all_captures
    )


def get_all_captures(board: Board(), color: str, is_sorted: bool = False):
    """The function returns a list of all possible captures for all figures."""
    row = col = board.get_size()
    result = []
    for i in range(row):
        for j in range(col):
            piece = board.get_cell(i, j)
            if piece:
                if piece.get_color() == color:
                    path_list = get_captures(board, i, j, is_sorted)
                    for path in path_list:
                        result.append(path)
    return sort_captures(result, is_sorted)


def apply_move(board: Board(), move: Tuple[str, str]) -> None:
    """Applies the player's moves and records the result in the field"""
    row, col = string_to_coordinates(move[0])
    row_end, col_end = string_to_coordinates(move[1])
    path_list = get_moves(board, row, col, is_sorted=False)

    if move[1] in path_list:
        piece = board.get_cell(row, col)
        if (
            piece.is_black()
            and row_end == board.get_size() - 1
            or piece.is_white()
            and row_end == 0
        ):
            piece.make_king()
        board.delete_cell(row, col)
        board.set_cell(row_end, col_end, piece)
    else:
        raise RuntimeError(
            "Invalid move, please type" + " 'hints' to get suggestions.\n\n"
        )


def apply_capture(board: Board(), capture_path: List[str]) -> None:
    """Applies the player's move removes the opponent's checker and records the result in the field"""
    count = 0
    while count < len(capture_path) - 1:
        path = [capture_path[count], capture_path[count + 1]]
        count += 1
        row, col = string_to_coordinates(path[0])
        row_end, col_end = string_to_coordinates(path[1])
        path_list = get_jumps(board, row, col, is_sorted=False)

        if path[1] in path_list:
            piece = board.get_cell(row, col)
            if (
                piece.is_black()
                and row_end == board.get_size() - 1
                or piece.is_white()
                and row_end == 0
            ):
                piece.make_king()
            board.delete_cell(row, col)
            row_eat, col_eat = max(row, row_end) - 1, max(col, col_end) - 1
            board.delete_cell(row_eat, col_eat)
            board.set_cell(row_end, col_end, piece)
        else:
            raise RuntimeError(
                "Invalid jump/capture, please type" + " 'hints' to get suggestions.\n\n"
            )


def get_hints(board: Board, color: str, is_sorted: bool = False) -> Tuple[List, List]:
    """Returns a tuple of lists:
    The first list contains all possible simple moves
    The second list contains all possible moves that lead to the destruction of the enemy pawn"""
    move = get_all_moves(board, color, is_sorted)
    jump = get_all_captures(board, color, is_sorted)
    if jump:
        return [], jump
    else:
        return move, jump


def get_winner(board: Board(), is_sorted: bool = False) -> str:
    """Calculates the winner and displays its color"""
    black_hint = get_hints(board, "black", is_sorted)
    white_hint = get_hints(board, "white", is_sorted)
    if black_hint != ([], []) and white_hint == ([], []):
        return "black"
    elif black_hint == ([], []) and white_hint != ([], []):
        return "white"
    else:
        black_king, white_king = 0, 0
        black, white = 0, 0
        row = col = board.get_size()
        for i in range(row):
            for j in range(col):
                piece = board.get_cell(i, j)
                if piece:
                    if piece.is_black():
                        black += 1
                        if piece.is_king():
                            black_king += 1
                    else:
                        white += 1
                        if piece.is_king():
                            white_king += 1
        if white_king == 1 and black_king == 1 and white == 1 and black == 1:
            return "draw"
        else:
            if white > black:
                return "white"
            elif black > white:
                return "black"
            else:
                return "draw"


def is_game_finished(board: Board(), is_sorted: bool = False) -> bool:
    """Boolean functian which calculates the winner and displays its color"""
    black_hint = get_hints(board, "black", is_sorted)
    white_hint = get_hints(board, "white", is_sorted)
    if black_hint == ([], []) or white_hint == ([], []):
        return True
    return False


move_error = "Invalid move, please type 'hints' to get suggestions.\n\n"
hasjump_error = "You have jumps, please type 'hints' to get suggestions.\n\n"
jump_error = "Invalid jump, please type 'hints' to get suggestions.\n\n"
cmd_error = "Invalid command.\n\n"
