import socket
import sys
import time

import minimax.minimax as minimax
from game.classes import Board, Figure

WHITE = 'white'
BLACK = 'black'
KING = ['B', 'W']
BLACK_FIG = ['b', 'B']
WHITE_FIG = ['w', 'W']
ROOM_ID = int(sys.argv[1])
PORT = 6666
HOST = "127.0.0.1"
BOT_ID = []


def get_best_move(board: Board, is_black: bool) -> tuple[tuple[str, str], bool]:
    cur_board = board
    best_move = minimax.minimax(cur_board, True, is_black)[1]
    return best_move


def change_bot_id(info: str):
    BOT_ID.append(info.split(' ')[2].split('\n')[0])


def process_result(previous_request: str, result: str) -> str:
    if previous_request == '':
        return 'my_info'
    if previous_request == 'my_info':
        change_bot_id(result)
        return "connect {}".format(ROOM_ID)
    elif "turn" in result or str(BOT_ID[-1]) in result:
        best_move = get_best_move(string_to_board(result), True if 'black' in result else False)
        if not best_move[1]:
            return 'move {} {}'.format(best_move[0][0], best_move[0][1])
        return 'jump {} {}'.format(best_move[0][0], best_move[0][1])
    elif 'available' in result or 'exist' in result:
        return "connect {}".format(ROOM_ID)
    elif 'This game ends in a draw' in result or 'wins' in result:
        return ''
    return '\n'


def string_to_board(board: str) -> Board:
    cur_board = Board()
    normal_board = []
    for el in board.split('|'):
        if el in ['  b  ', '  B  ', '  w  ', '  W  ', '     ']:
            normal_board.append(el.replace(' ', ''))
    for i, el in enumerate(normal_board):
        is_king = False
        el = el.replace(' ', '').replace('\n', '')
        if el in KING:
            is_king = True
        if el == 'b' or el == 'B':
            cur_board.set_cell(i // 8, i % 8, Figure(BLACK, is_king))
        if el == 'w' or el == 'W':
            cur_board.set_cell(i // 8, i % 8, Figure(WHITE, is_king))

    return cur_board


def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))

        read_file = s.makefile(mode='r', encoding="utf-8")
        write_file = s.makefile(mode='w', encoding="utf-8")
        previous_request = ""
        while True:
            result = read_file.readline().strip()
            while True:
                data = read_file.readline().strip()
                if data == "":
                    break
                result += '\n' + data
            request = process_result(previous_request, result)
            if request == "":
                break
            time.sleep(3)
            write_file.write(request + '\n')
            write_file.flush()
            previous_request = request


if __name__ == "__main__":
    main()
