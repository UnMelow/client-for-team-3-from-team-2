from game.classes import Board, Figure
from game.main import apply_capture, is_game_finished, get_winner, initialize
from minimax.minimax import minimax, get_all_moves


def test_get_possible_move():
    board = Board()
    initialize(board)
    assert get_all_moves('white', board)
