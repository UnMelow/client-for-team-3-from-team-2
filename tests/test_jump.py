from game.classes import Board, Figure
from game.main import apply_capture, is_game_finished, get_winner
from minimax.minimax import minimax

BOARD_CELLS = [
        [None, Figure(), None, Figure(), None, Figure(), None, Figure()],
        [Figure(), None, Figure(), None, Figure(), None, Figure(), None],
        [None, Figure(), None, Figure(), None, None, None, Figure()],
        [None, None, None, None, Figure(), None, None, None],
        [None, None, None, Figure("white"), None, None, None, None],
        [Figure("white"), None, None, None, Figure("white"), None, Figure("white"), None],
        [None, Figure("white"), None, Figure("white"), None, Figure("white"), None, Figure("white")],
        [Figure("white"), None, Figure("white"), None, Figure("white"), None, Figure("white"), None],
    ]


def test_apply_capture():
    board = Board()
    board._cells = BOARD_CELLS
    move = minimax(board, True)
    is_jump = move[1][1]
    apply_capture(board=board, capture_path=["d5", "f3"])
    assert is_jump
    assert board.display() == "      1     2     3     4     5     6     7     8 \n   +-----+-----+-----+-----+-----+-----+-----+-----+\na  |     |  b  |     |  b  |     |  b  |     |  b  |\n   +-----+-----+-----+-----+-----+-----+-----+-----+\nb  |  b  |     |  b  |     |  b  |     |  b  |     |\n   +-----+-----+-----+-----+-----+-----+-----+-----+\nc  |     |  b  |     |  b  |     |     |     |  b  |\n   +-----+-----+-----+-----+-----+-----+-----+-----+\nd  |     |     |     |     |     |     |     |     |\n   +-----+-----+-----+-----+-----+-----+-----+-----+\ne  |     |     |     |     |     |     |     |     |\n   +-----+-----+-----+-----+-----+-----+-----+-----+\nf  |  w  |     |  b  |     |  w  |     |  w  |     |\n   +-----+-----+-----+-----+-----+-----+-----+-----+\ng  |     |  w  |     |  w  |     |  w  |     |  w  |\n   +-----+-----+-----+-----+-----+-----+-----+-----+\nh  |  w  |     |  w  |     |  w  |     |  w  |     |\n   +-----+-----+-----+-----+-----+-----+-----+-----+\n"
