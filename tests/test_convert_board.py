from game.classes import Board, Figure
from game.main import initialize

WHITE = 'white'
BLACK = 'black'
KING = ['B', 'W']
BLACK_FIG = ['b', 'B']
WHITE_FIG = ['w', 'W']


def string_to_board(board: str) -> Board:
    cur_board = Board()
    normal_board = []
    for el in board.split('|'):
        if el in ['  b  ', '  B  ', '  w  ', '  W  ', '     ']:
            normal_board.append(el.replace(' ', ''))
    for i, el in enumerate(normal_board):
        is_king = False
        el = el.replace(' ', '').replace('\n', '')
        if el in KING:
            is_king = True
        if el == 'b' or el == 'B':
            cur_board.set_cell(i // 8, i % 8, Figure(BLACK, is_king))
        if el == 'w' or el == 'W':
            cur_board.set_cell(i // 8, i % 8, Figure(WHITE, is_king))

    return cur_board


def test_convert():
    board = Board()
    initialize(board)
    board = string_to_board(str(board))
    assert board
    assert type(board) == Board
